//
//  MenuView.swift
//  AppetiteTest
//
//  Created by Алишер Халыкбаев on 27.08.2021.
//

import UIKit
import Hue

class MenuView: UIView {
    
    init() {
        super.init(frame: .zero)
        backgroundColor = UIColor.init(hex: "#FAFAFA")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
