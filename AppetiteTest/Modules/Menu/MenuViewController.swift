//
//  MenuViewController.swift
//  AppetiteTest
//
//  Created by Алишер Халыкбаев on 27.08.2021.
//

import UIKit

class MenuViewController: UIViewController {
    
    let rootView: MenuView
    
    init() {
        self.rootView = MenuView()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK:- View Lifecycle
extension MenuViewController {
    
    override func loadView() {
        super.view = rootView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
