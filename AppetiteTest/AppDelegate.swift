//
//  AppDelegate.swift
//  AppetiteTest
//
//  Created by Алишер Халыкбаев on 27.08.2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow.init()
        window?.rootViewController = MenuViewController()
        window?.makeKeyAndVisible()
        
        return true
    }

}

