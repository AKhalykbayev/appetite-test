//
//  RestaurantApi.swift
//  AppetiteTest
//
//  Created by Алишер Халыкбаев on 27.08.2021.
//

import Foundation
import Moya

enum RestaurantApi {
    case fetchMenu
}

extension RestaurantApi: TargetType {
    
    var baseURL: URL {
        return URL(string: "https://cheflist.org/api/v1/")!
    }
    
    var path: String {
        switch self {
        case .fetchMenu:
            return "menu/"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .fetchMenu:
            return .get
        }
    }
    
    var task: Task {
        var parameters: [String: Any] = [:]
        
        switch self {
        case .fetchMenu:
            parameters["id_restaurant"] = 72
            parameters["type"] = 2
        }
        
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var headers: [String: String]? {
        let parameters: [String: String] = [
            "Accept-Language": "ru",
            "platform": "ios",
            "Timezone": "Asia/Almaty",
            "Authorization": "Bearer d920abd15b664613533a26c08e40e978b084d11d4411c6ed77a97cf79f8033d7858f6c0d2dbc501b5c89d86c3f388fc0603507cbb5856260db9ea0444ab305053081b29eb28b030e3cdcf73653c03c08569e42ae554477181fd8825116e9856c5c665ef0e682d8365ac990e2e0becc1a107bb0dec81feb6ff299",
            "User-Agent": "x86_64 | version: 2.2, build: 28, lib: Alamofire/5.4.3"
        ]
        return parameters
    }
}

